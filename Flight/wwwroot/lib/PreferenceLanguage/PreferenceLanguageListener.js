// Generated from PreferenceLanguage.g4 by ANTLR 4.7.1
// jshint ignore: start
var antlr4 = require('antlr4/index');

// This class defines a complete listener for a parse tree produced by PreferenceLanguageParser.
function PreferenceLanguageListener() {
	antlr4.tree.ParseTreeListener.call(this);
	return this;
}

PreferenceLanguageListener.prototype = Object.create(antlr4.tree.ParseTreeListener.prototype);
PreferenceLanguageListener.prototype.constructor = PreferenceLanguageListener;

// Enter a parse tree produced by PreferenceLanguageParser#preferenceSet.
PreferenceLanguageListener.prototype.enterPreferenceSet = function(ctx) {
};

// Exit a parse tree produced by PreferenceLanguageParser#preferenceSet.
PreferenceLanguageListener.prototype.exitPreferenceSet = function(ctx) {
};


// Enter a parse tree produced by PreferenceLanguageParser#setFormat1.
PreferenceLanguageListener.prototype.enterSetFormat1 = function(ctx) {
};

// Exit a parse tree produced by PreferenceLanguageParser#setFormat1.
PreferenceLanguageListener.prototype.exitSetFormat1 = function(ctx) {
};


// Enter a parse tree produced by PreferenceLanguageParser#setFormat2.
PreferenceLanguageListener.prototype.enterSetFormat2 = function(ctx) {
};

// Exit a parse tree produced by PreferenceLanguageParser#setFormat2.
PreferenceLanguageListener.prototype.exitSetFormat2 = function(ctx) {
};


// Enter a parse tree produced by PreferenceLanguageParser#setFormat3.
PreferenceLanguageListener.prototype.enterSetFormat3 = function(ctx) {
};

// Exit a parse tree produced by PreferenceLanguageParser#setFormat3.
PreferenceLanguageListener.prototype.exitSetFormat3 = function(ctx) {
};


// Enter a parse tree produced by PreferenceLanguageParser#preference.
PreferenceLanguageListener.prototype.enterPreference = function(ctx) {
};

// Exit a parse tree produced by PreferenceLanguageParser#preference.
PreferenceLanguageListener.prototype.exitPreference = function(ctx) {
};



exports.PreferenceLanguageListener = PreferenceLanguageListener;