﻿require.config({

        urlArgs: "bust=v2",
        paths: {
               antlr4: "../lib/antlr4/index",
               lexer: "../lib/PreferenceLanguage/PreferenceLanguageLexer",
               parser: "../lib/PreferenceLanguage/PreferenceLanguageParser",
               listener: "../lib/PreferenceLanguage/PreferenceLanguageListener",
        }

});
