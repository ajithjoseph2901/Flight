// Generated from ../../Antlr/PreferenceLanguage.g4 by ANTLR 4.7.1
// jshint ignore: start
var antlr4 = require('antlr4/index');


var serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964",
    "\u0002\bF\b\u0001\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004",
    "\t\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0004\u0007\t\u0007",
    "\u0003\u0002\u0003\u0002\u0003\u0003\u0003\u0003\u0003\u0004\u0003\u0004",
    "\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0005\u0003\u0005",
    "\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005",
    "\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005",
    "\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005",
    "\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005",
    "\u0003\u0005\u0003\u0005\u0003\u0005\u0003\u0005\u0005\u00058\n\u0005",
    "\u0003\u0006\u0007\u0006;\n\u0006\f\u0006\u000e\u0006>\u000b\u0006\u0003",
    "\u0007\u0006\u0007A\n\u0007\r\u0007\u000e\u0007B\u0003\u0007\u0003\u0007",
    "\u0002\u0002\b\u0003\u0003\u0005\u0004\u0007\u0005\t\u0006\u000b\u0007",
    "\r\b\u0003\u0002\u0004\u0003\u0002c|\u0004\u0002\u000b\u000b\"\"\u0002",
    "L\u0002\u0003\u0003\u0002\u0002\u0002\u0002\u0005\u0003\u0002\u0002",
    "\u0002\u0002\u0007\u0003\u0002\u0002\u0002\u0002\t\u0003\u0002\u0002",
    "\u0002\u0002\u000b\u0003\u0002\u0002\u0002\u0002\r\u0003\u0002\u0002",
    "\u0002\u0003\u000f\u0003\u0002\u0002\u0002\u0005\u0011\u0003\u0002\u0002",
    "\u0002\u0007\u0013\u0003\u0002\u0002\u0002\t7\u0003\u0002\u0002\u0002",
    "\u000b<\u0003\u0002\u0002\u0002\r@\u0003\u0002\u0002\u0002\u000f\u0010",
    "\u0007*\u0002\u0002\u0010\u0004\u0003\u0002\u0002\u0002\u0011\u0012",
    "\u0007+\u0002\u0002\u0012\u0006\u0003\u0002\u0002\u0002\u0013\u0014",
    "\u0007C\u0002\u0002\u0014\u0015\u0007F\u0002\u0002\u0015\u0016\u0007",
    "U\u0002\u0002\u0016\u0017\u0007H\u0002\u0002\u0017\u0018\u0007C\u0002",
    "\u0002\u0018\b\u0003\u0002\u0002\u0002\u0019\u001a\u0007C\u0002\u0002",
    "\u001a\u001b\u0007P\u0002\u0002\u001b8\u0007F\u0002\u0002\u001c\u001d",
    "\u0007c\u0002\u0002\u001d\u001e\u0007p\u0002\u0002\u001e8\u0007f\u0002",
    "\u0002\u001f \u0007Q\u0002\u0002 8\u0007T\u0002\u0002!\"\u0007q\u0002",
    "\u0002\"8\u0007t\u0002\u0002#$\u0007e\u0002\u0002$%\u0007q\u0002\u0002",
    "%&\u0007o\u0002\u0002&\'\u0007r\u0002\u0002\'(\u0007t\u0002\u0002()",
    "\u0007q\u0002\u0002)*\u0007o\u0002\u0002*+\u0007k\u0002\u0002+,\u0007",
    "u\u0002\u0002,8\u0007g\u0002\u0002-.\u0007E\u0002\u0002./\u0007Q\u0002",
    "\u0002/0\u0007O\u0002\u000201\u0007R\u0002\u000212\u0007T\u0002\u0002",
    "23\u0007Q\u0002\u000234\u0007O\u0002\u000245\u0007K\u0002\u000256\u0007",
    "U\u0002\u000268\u0007G\u0002\u00027\u0019\u0003\u0002\u0002\u00027\u001c",
    "\u0003\u0002\u0002\u00027\u001f\u0003\u0002\u0002\u00027!\u0003\u0002",
    "\u0002\u00027#\u0003\u0002\u0002\u00027-\u0003\u0002\u0002\u00028\n",
    "\u0003\u0002\u0002\u00029;\t\u0002\u0002\u0002:9\u0003\u0002\u0002\u0002",
    ";>\u0003\u0002\u0002\u0002<:\u0003\u0002\u0002\u0002<=\u0003\u0002\u0002",
    "\u0002=\f\u0003\u0002\u0002\u0002><\u0003\u0002\u0002\u0002?A\t\u0003",
    "\u0002\u0002@?\u0003\u0002\u0002\u0002AB\u0003\u0002\u0002\u0002B@\u0003",
    "\u0002\u0002\u0002BC\u0003\u0002\u0002\u0002CD\u0003\u0002\u0002\u0002",
    "DE\b\u0007\u0002\u0002E\u000e\u0003\u0002\u0002\u0002\u0006\u00027<",
    "B\u0003\b\u0002\u0002"].join("");


var atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

var decisionsToDFA = atn.decisionToState.map( function(ds, index) { return new antlr4.dfa.DFA(ds, index); });

function PreferenceLanguageLexer(input) {
	antlr4.Lexer.call(this, input);
    this._interp = new antlr4.atn.LexerATNSimulator(this, atn, decisionsToDFA, new antlr4.PredictionContextCache());
    return this;
}

PreferenceLanguageLexer.prototype = Object.create(antlr4.Lexer.prototype);
PreferenceLanguageLexer.prototype.constructor = PreferenceLanguageLexer;

Object.defineProperty(PreferenceLanguageLexer.prototype, "atn", {
        get : function() {
                return atn;
        }
});

PreferenceLanguageLexer.EOF = antlr4.Token.EOF;
PreferenceLanguageLexer.T__0 = 1;
PreferenceLanguageLexer.T__1 = 2;
PreferenceLanguageLexer.NAME = 3;
PreferenceLanguageLexer.SEPARATOR = 4;
PreferenceLanguageLexer.TEXT = 5;
PreferenceLanguageLexer.WHITESPACE = 6;

PreferenceLanguageLexer.prototype.channelNames = [ "DEFAULT_TOKEN_CHANNEL", "HIDDEN" ];

PreferenceLanguageLexer.prototype.modeNames = [ "DEFAULT_MODE" ];

PreferenceLanguageLexer.prototype.literalNames = [ null, "'('", "')'", "'ADSFA'" ];

PreferenceLanguageLexer.prototype.symbolicNames = [ null, null, null, "NAME", 
                                                    "SEPARATOR", "TEXT", 
                                                    "WHITESPACE" ];

PreferenceLanguageLexer.prototype.ruleNames = [ "T__0", "T__1", "NAME", 
                                                "SEPARATOR", "TEXT", "WHITESPACE" ];

PreferenceLanguageLexer.prototype.grammarFileName = "PreferenceLanguage.g4";



exports.PreferenceLanguageLexer = PreferenceLanguageLexer;

